<?php ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="author" content="Miki Arsov">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>CRUD App</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <!-- Popper JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css" />

        <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>
    </head>

    <body>
        <nav class="navbar navbar-expand-md bg-dark navbar-dark">
            <!-- Brand -->
            <a class="navbar-brand" href="#">Products</a>
            <!-- Toggler/collapsibe Button -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                <span class="navbar-toggler-icon"></span>
            </button>
            <!-- Navbar links -->
            <div class="collapse navbar-collapse" id="collapsibleNavbar">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="#">Features</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Services</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">About</a>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        <h5 class="card-header">Add Product</h5>
                        <?php
                        include 'model.php';
                        $model = new Model();
                        $insert = $model->insert();
                        ?>
                        <form action="" method="POST" enctype="multipart/form-data">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <label for="product_slug">SKU</label>
                                        <input type="text" class="form-control" id="product_slug_db" name="product_slug_db" hidden>
                                        <input type="text" class="form-control" id="product_slug" name="product_slug" placeholder="Please, submit required data." required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                        <label for="product_name">Name</label>
                                        <input type="text" class="form-control" id="product_name" name="product_name" placeholder="Please, submit required data." required>
                                    </div>
                                </div> 
                                <div class="row">
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                        <label for="product_price">Price ($)</label>
                                        <input type="number" class="form-control" id="product_price" name="product_price" placeholder="Please, submit required data." required >
                                    </div>
                                </div> 
                                <div class="row">
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                        <label for="product_type">Type Switcher</label>
                                        <select id='product_type' name="product_type" class="form-control">
                                            <option value="0">Choose type of Product</option>
                                            <option value="1">DVD</option>
                                            <option value="2">Book </option>
                                            <option value="3">Furniture</option>
                                        </select>
                                        <br>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 ">
                                        <div id="dvddisk">
                                            <div class="form-group">
                                                <label class="control-label" for="product_desc1">Size (in MB)</label>
                                                <input class="form-control" id="product_desc1" name="product_desc1" type="text" placeholder="Please, provide size">
                                            </div>
                                        </div>
                                        <div id="book">
                                            <div class="form-group">
                                                <label class="control-label" for="product_desc2">Weight (in Kg))</label>
                                                <input class="form-control" id="product_desc2" name="product_desc2" type="text" placeholder="Please, provide weight">
                                            </div>
                                        </div>
                                        <div id="furniture">
                                            <label class="control-label" for="product_desc">Dimensions (HxWxL)</label>
                                            <input class="form-control" id="product_desc" name="product_desc" type="text" placeholder="Please, provide dimensions">
                                        </div>
                                    </div>
                                </div> 
                                <div class="row pt-2 pt-sm-2 mt-1">
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <p class="text-right">
                                            <button type="submit" name="submit" class="btn btn-space btn-primary">Save</button>
                                            <button type="reset" onclick="history.back()" class="btn btn-space btn-secondary">Cancel</button>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
<script>
    $(document).ready(function ($) {
        $("#dvddisk").hide();
        $("#book").hide();
        $("#furniture").hide();
        $('#product_type').on('change', function () {
            switch ($("#product_type").val()) {
                case "1":
                    $("#book").hide();
                    $("#furniture").hide();
                    $("#dvddisk").show();
                    break;
                case "2":
                    $("#book").show();
                    $("#dvddisk").hide();
                    $("#furniture").hide();
                    break;
                case "3":
                    $("#furniture").show();
                    $("#dvddisk").hide();
                    $("#book").hide();
                    break;

                default:
                    break;
            }
        });

    });
</script>