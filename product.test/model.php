<?php

Class Model {

    private $server = "localhost";
    private $username = "root";
    private $password;
    private $db = "product_db";
    private $conn;

    public function __construct() {
        try {

            $this->conn = new mysqli($this->server, $this->username, $this->password, $this->db);
        } catch (Exception $e) {
            echo "connection failed" . $e->getMessage();
        }
    }

    public function insert() {

        if (isset($_POST['submit'])) {
            if (isset($_POST['product_slug']) && isset($_POST['product_name']) && isset($_POST['product_type']) && isset($_POST['product_price']) && isset($_POST['product_desc']) && isset($_POST['product_desc1']) && isset($_POST['product_desc2'])) {

                $product_slug = $_POST['product_slug'];
                $product_name = $_POST['product_name'];
                $product_desc = $_POST['product_desc'];
                $product_desc1 = $_POST['product_desc1'];
                $product_desc2 = $_POST['product_desc2'];
                $product_type = $_POST['product_type'];
                $product_price = $_POST['product_price'];


                $query = "INSERT INTO products (product_slug,product_name,product_desc,product_desc1,product_desc2,product_type,product_price) VALUES ('$product_slug','$product_name','$product_desc','$product_desc1','$product_desc2','$product_type','$product_price')";
                if ($sql = $this->conn->query($query)) {

                    echo "<script>window.location.href = 'products.php';</script>";
                }
            }
        }
    }
    public function fetch() {
        $data = null;

        $query = "SELECT * FROM products ORDER BY product_id desc";
        if ($sql = $this->conn->query($query)) {
            while ($row = mysqli_fetch_assoc($sql)) {
                $data[] = $row;
            }
        }

        return $data;
    }

    public function delete($id) {

        $query = "DELETE FROM products where product_id = '$id'";
        if ($sql = $this->conn->query($query)) {
            return true;
        } else {
            return false;
        }
    }

}

?>