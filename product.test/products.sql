-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 23, 2021 at 10:23 PM
-- Server version: 5.5.60-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `product_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `product_id` int(11) NOT NULL,
  `product_slug` varchar(250) NOT NULL,
  `product_name` varchar(250) NOT NULL,
  `product_desc` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `product_desc1` text,
  `product_desc2` text,
  `product_type` int(11) DEFAULT NULL,
  `product_time` datetime NOT NULL,
  `product_price` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `product_slug`, `product_name`, `product_desc`, `product_desc1`, `product_desc2`, `product_type`, `product_time`, `product_price`) VALUES
(44, 'table-home', 'Table', '120x45x60', '', '', 3, '2021-05-21 16:50:12', '630'),
(45, 'table-home-2', 'Table Black', '120x45x60', '', '', 3, '2021-05-21 16:52:44', '150'),
(47, 'table-home-3', 'Table White', '150x45x60', '', '', 3, '2021-05-21 16:58:00', '505'),
(61, 'Chair-220', 'Chair black', '24x45x15', '', '', 3, '2021-05-21 17:22:41', '25'),
(62, 'book-220', 'Book black', '24x45x15', '', '', 2, '2021-05-21 17:23:10', '50'),
(67, 'table-homepo', 'Table White 3', '150x45x80', '', '', 3, '2021-05-21 17:27:09', '505'),
(72, 'table-home-22', 'Table White 43', '200x45x60', '', '', 3, '2021-05-21 21:19:04', '630'),
(73, 'table-home-42', 'Table White 53', '200x65x60', '', '', 3, '2021-05-21 21:19:17', '620'),
(75, 'table-home-322', 'Table Blue', '150x45x60', '', '', 3, '2021-05-21 21:48:30', '150'),
(79, 'bedside-lamp', 'Bedside Night light', '15x15x20', '', '', 3, '2021-05-21 21:56:18', '35'),
(82, 'Table-for-home', 'Table Black & Blue', '250x45x60', '', '', 3, '2021-05-21 22:08:16', '355'),
(83, 'Table-for-home2223', 'Table Black & Blue', '250x45x60', '', '', 3, '2021-05-21 22:10:35', '50'),
(94, 'book-for-homewdadaw', 'Book', '', '', '2kg', 2, '2021-05-21 23:04:52', '50'),
(96, 'dvd-for-watch', 'Cd with Music', '', '140MB', '', 1, '2021-05-21 23:06:57', '15'),
(97, 'book-for-home222111', 'Book of Wonders', '', '', '1KG', 2, '2021-05-21 23:07:17', '50'),
(98, 'Dvd-cd-dvd221', 'Dvd with Movies', '', '1200MB', '', 1, '2021-05-21 23:07:56', '15'),
(99, 'book-for-home13', 'Book of Sports', '', '', '1.5KG', 2, '2021-05-21 23:08:18', '27'),
(100, 'book-for-home222333', 'BookUI', '', '', '2kg', 2, '2021-05-21 23:11:08', '50'),
(101, 'Dvd-cd-dvd2212', 'Dvd with Stuff', '', '700MB', '', 1, '2021-05-21 23:11:30', '15'),
(102, 'book-for-home', 'Book', '', '', '1KG', 2, '2021-05-21 23:18:16', '50'),
(103, 'book-for-home22', 'Book', '', '', '1KG', 2, '2021-05-22 10:41:48', '50'),
(104, 'book-for-home2222', 'Book21', '', '', '2kg', 2, '2021-05-22 10:46:31', '15'),
(105, 'Table-for-home12', 'Table Black & Yellow', '250x45x60', '', '', 3, '2021-05-22 10:48:40', '630'),
(106, 'Dvd-cd-112312', 'Dvd Movie ', '', '1200MB', '', 1, '2021-05-22 10:49:19', '50'),
(107, 'book-for-home1223', 'Book', '', '', '1.5KG', 2, '2021-05-22 10:50:12', '50'),
(108, 'Chair-2202', 'Chair black', '24x45x15', '', '', 3, '2021-05-22 10:51:06', '50'),
(109, 'Chair-301', 'Chair black', '24x45x15', '', '', 3, '2021-05-22 10:54:40', '15'),
(110, 'Chair-22022', 'Chair white', '24x45x15', '', '', 3, '2021-05-22 11:17:52', '50'),
(111, 'Chair-3012', 'Chair white', '24x45x15', '', '', 3, '2021-05-22 12:32:35', '25'),
(112, 'book-for-water', 'Book of Water', '', '', '1KG', 2, '2021-05-22 14:53:26', '35'),
(113, 'book-for-champions', 'Book of Championz', '', '', '3KG', 2, '2021-05-22 15:40:03', '50'),
(114, 'book-for-home2138', 'Book', '', '', '1KG', 2, '2021-05-23 12:06:10', '50'),
(115, 'book-for-home90', 'Book for numbers', '', '', '3KG', 2, '2021-05-23 14:00:17', '50'),
(116, 'Dvd-cd-90', 'Dvd Movie Horror', '', '2000MB', '', 1, '2021-05-23 14:14:46', '50'),
(117, 'Chair-99-promo', 'Chair x6 Dining Table x1 White', '250x45x60', '', '', 3, '2021-05-23 15:21:31', '1120');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`),
  ADD UNIQUE KEY `product_slug` (`product_slug`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=118;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
