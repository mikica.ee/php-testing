<?php

require_once ('product.php');
require_once ('interface.php');

Class Class_type extends Product implements Product_Interface {

    public function __construct() {

        $res = $this->dbConnect();
    }

    public function getList() {

        $query = "SELECT * FROM products ORDER BY product_id desc";
        $res = $this->getResultSetArray($query);

        return $res;
    }

    public function insert() {
        if (isset($_POST['submit'])) {


            $product_slug = $_POST['product_slug'];
            $product_name = $_POST['product_name'];
            $product_type = $_POST['product_type'];
            $product_price = $_POST['product_price'];
            $dimensions = $_POST['dimensions'];
            $size = $_POST['size'];
            $weight = $_POST['weight'];
            $query = "INSERT INTO products (product_slug,product_name,dimensions,size,weight,product_type,product_price) VALUES ('$product_slug','$product_name','$dimensions','$size','$weight','$product_type','$product_price')";
            if ($sql = $this->conn->query($query)) {
                echo "<script>window.location.href = 'products.php';</script>";
            }
        }
    }

    public function delete($id) {
        $query = "DELETE FROM products where product_id = '$id'";
        if ($sql = $this->conn->query($query)) {
            return true;
        } else {
            return false;
        }
    }

}

class Book extends Product implements Product_description_interface {

    private $weight;

    public function product_desc() {
        return $this->weight;
        
    }

}

class Dvd extends Product implements Product_description_interface {

    private $size;

    public function product_desc() {
        return $this->size;
    }

}

class Furniture extends Product implements Product_description_interface {

    private $dimensions;

    public function product_desc() {
        return $this->dimensions;
    }

}
