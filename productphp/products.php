<?php ?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="author" content="Miki Arsov">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Product App - Miki</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <!-- Popper JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css" />

        <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>
    </head>
    <body>
        <div class="dashboard-wrapper">
            <div class="container-fluid  dashboard-content">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <h1 class="text-center">Products List</h1>
                        <hr style="height: 1px;color: black;background-color: black;">
                    </div>
                </div>
                <div class="row text-center" >    
                    <div class="row pt-6 pt-sm-6 mt-6">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 offset-1">
                            <form method="post" action="index.php">
                                <button type="submit" class="btn btn-space btn-primary">Add Product</button>
                                <button type="button" id="delete.php" class="btn btn-space btn-secondary">Mass Delete</button>
                            </form>
                        </div>
                    </div>
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="row">
                            <?php
                            include 'class_type.php';
                            $class_type = new Class_type();
                            $allProducts = 0;
                            $products = [
                                new Book(),
                                new Dvd(),
                                new Furniture()
                            ];
                            $res = $class_type->getList($allProducts);
                            if (isset($res['productList'])) {
                                foreach ($res['productList'] as $result) {
                                    foreach ($products as $product) {
                                        ?>
                                        <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
                                            <div class="product-thumbnail">
                                                <div class="product-content">  
                                                    <div class="product-content-head">

                                                        <h3 class="product_slug"><?= $result['product_slug']; ?></h3>
                                                        <h3 class="product-title"><?= $result['product_name']; ?></h3>
                                                        <div class="product_price"><?= $result['product_price']; ?> $</div>
                                                        <div class="size"><?= $result['size'];?></div>
                                                        <div class="dimensions"><?= $result['dimensions'];?></div>
                                                        <div class="weight"><?= $result['weight'];?></div>
                                                        <a href="delete.php?id=<?= $result['product_id']; ?>" class="btn btn-space btn-secondary">Delete</a>
                                                        <div class="checkbox">
                                                            <label class="checkbox">
                                                                <input  value="<?= $result['product_id'] ?>" type="checkbox"></label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>
</html>
