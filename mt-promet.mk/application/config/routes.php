<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['villas'] = 'client/services/villas/$1';
$route['yachts'] = 'client/services/yachts/$1';
$route['carrental'] = 'client/services/carrental/$1';
$route['chauffeur'] = 'client/services/chauffeur/$1';
$route['itineraries'] = 'client/services/itineraries/$1';
$route['extraservices'] = 'client/services/extraservices/$1';
$route['concierge'] = 'client/services/concierge/$1';
$route['service/(:any)'] = 'client/services/service/$1';
$route['transport'] = 'client/pages/transport';
$route['checkout/(:any)'] = 'client/checkout/checkout/$1';
$route['offer/(:any)'] = 'client/checkout/offer/$1';
$route['cart'] = 'client/cart/cart';

//pages
$route['terms'] = 'client/pages/terms';
$route['villaterms'] = 'client/pages/villaterms';
$route['yachtterms'] = 'client/pages/yachtterms';
$route['contact'] = 'client/contact/contact';

$route['sitemap\.xml'] = "sitemap/index";
$route['404_override'] = 'home/err404';
$route['translate_uri_dashes'] = FALSE;