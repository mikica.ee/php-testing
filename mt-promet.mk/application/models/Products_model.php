<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User_model class.
 *
 * @extends CI_Model
 */
class Products_model extends CI_Model {

    public function __construct() {

        parent::__construct();
        $this->load->database();
    }

   

    public function insert($data) {
        return $this->db->insert('products', $data);
    }

    public function select() {
        $this->db->select('*');
        $this->db->from('products');
        $this->db->order_by("product_time", "desc");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function select_from_id($id) {
        $this->db->select('*');
        $this->db->from('products');
        $this->db->where('product_id', $id);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function select_from_slug($slug) {
        $this->db->select('*');
        $this->db->from('products');
        $this->db->where('product_slug', $slug);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function delete($id) {
        return $this->db->delete('products', array('product_id' => $id));
        
    }
}
