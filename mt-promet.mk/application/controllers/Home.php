<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->library(array('session'));
        $this->load->helper(array('url'));
        $this->load->model(array('products_model'));
    }

    public function index() {
        $this->load->view('header');
        $this->load->view('footer');
    }


    public function err404() {
        $this->load->view('header');
        $this->load->view('err404');
        $this->load->view('footer');
    }

}
