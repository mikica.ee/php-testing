<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends CI_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->library(array('session'));
        $this->load->helper(array('url'));
        $this->load->model(array('products_model'));
    }

    public function index() {

        $this->load->view('header');
        $data['products'] = $this->products_model->select();
        $this->load->view('products/list', $data);
        $this->load->view('footer');
    }

    public function slug_check() {

        $product = $this->products_model->select_from_slug($this->input->post('product_slug'));
        if (!empty($product)) {
            echo 1;
        } else {
            echo $this->input->post('product_slug');
        }
    }

    public function insert_product() {

        $data['product_slug'] = $this->input->post('product_slug');
        $data['product_name'] = $this->input->post('product_name');
        $data['product_desc'] = $this->input->post('product_desc');
        $data['product_desc1'] = $this->input->post('product_desc1');
        $data['product_desc2'] = $this->input->post('product_desc2');
        $data['product_type'] = $this->input->post('product_type');
        $data['product_price'] = $this->input->post('product_price');
        $data['product_time'] = date('Y-m-j H:i:s');
        $this->products_model->insert($data);
        $insert_id = $this->products_model->db->insert_id();
    }

    public function delete() {
        if ($this->products_model->delete($this->input->post('product_id'))) {
                $this->input->post('product_id');
                echo 'Deleted';
            } else {
                echo 'Error try again';
            }
    
        
    }

    public function insert() {

        $this->load->view('header');
        $data['products'] = $this->products_model->select();
        $this->load->view('products/insert', $data);
        $this->load->view('footer');
    }

}
