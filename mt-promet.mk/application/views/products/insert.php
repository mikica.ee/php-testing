<div class="dashboard-wrapper">
    <div class="container-fluid  dashboard-content">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title">Insert Product </h2>
                </div>
            </div>
        </div>
        <form method="post" enctype="multipart/form-data" id="insertproduct" action="javascript:;" name="insertproduct" >
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        <h5 class="card-header">Add Product</h5>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <label for="product_slug">SKU</label>
                                    <input type="text" class="form-control" id="product_slug_db" name="product_slug_db" hidden>
                                    <input type="text" class="form-control" id="product_slug" name="product_slug">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                    <label for="product_name">Name</label>
                                    <input type="text" class="form-control" id="product_name" name="product_name">
                                </div>
                            </div> 
                            <div class="row">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                    <label for="product_price">Price ($)</label>
                                    <input type="text" class="form-control" id="product_price" name="product_price">
                                </div>
                            </div> 
                            <div class="row">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                    <label for="product_type">Type Switcher</label>
                                    <select id='product_type' name="product_type" class="form-control">
                                        <option value="0">Choose type of Product</option>
                                        <option value="1">DVD</option>
                                        <option value="2">Book </option>
                                        <option value="3">Furniture</option>
                                    </select>
                                    <br>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 ">
                                    <div id="dvddisk">
                                        <div class="form-group">
                                            <label class="control-label" for="product_desc1">Size (in MB)</label>
                                            <input class="form-control" id="product_desc1" name="product_desc1" type="text" placeholder="Please, provide size">
                                        </div>
                                    </div>
                                    <div id="book">
                                        <div class="form-group">
                                            <label class="control-label" for="product_desc2">Weight (in Kg))</label>
                                            <input class="form-control" id="product_desc2" name="product_desc2" type="text" placeholder="Please, provide weight">
                                        </div>
                                    </div>
                                    <div id="furniture">
                                        <label class="control-label" for="product_desc">Dimensions (HxWxL)</label>
                                        <input class="form-control" id="product_desc" name="product_desc" type="text" placeholder="Please, provide dimensions">
                                    </div>
                                </div>
                            </div> 
                            <div class="row pt-2 pt-sm-2 mt-1">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <p class="text-right">
                                        <button type="submit" class="btn btn-space btn-primary">Save</button>
                                        <button type="reset" onclick="history.back()" class="btn btn-space btn-secondary">Cancel</button>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $(document).ready(function ($) {
        $("#product_slug").keyup(function () {
            var product = new FormData($("#insertproduct")[0]);
            $.ajax({
                url: '<?= site_url('products/slug_check') ?>',
                type: 'POST',
                data: product,
                success: function (data) {
                    $("#product_slug_db").val(data);
                },
                cache: false,
                contentType: false,
                processData: false
            });
        });
        $("#dvddisk").hide();
        $("#book").hide();
        $("#furniture").hide();
        $('#product_type').on('change', function () {
            switch ($("#product_type").val()) {
                case "1":
                    $("#book").hide();
                    $("#furniture").hide();
                    $("#dvddisk").show();
                    break;
                case "2":
                    $("#book").show();
                    $("#dvddisk").hide();
                    $("#furniture").hide();
                    break;
                case "3":
                    $("#furniture").show();
                    $("#dvddisk").hide();
                    $("#book").hide();
                    break;

                default:
                    break;
            }
        });
        $('form[id="insertproduct"]').validate({
            errorClass: 'is-invalid',
            validClass: 'is-valid',
            rules: {
                product_name: 'required',
                product_type: 'required',
                product_price: 'required',
                product_slug: {
                    required: true,
                    minlength: 3,
                    equalTo: "#product_slug_db"
                },
                product_desc: 'required',
                product_desc1: 'required',
                product_desc2: 'required',
            },
            messages: {
                product_name: 'Please, submit required data.',
                product_desc: 'Please, submit required data.',
                product_desc1: 'Please, submit required data.',
                product_desc2: 'Please, submit required data.',
                product_type: 'Please, submit required data.',
                product_price: 'Please, submit required data.',
                product_slug: {
                    required: 'Please, submit required data.',
                    minlength: 'min 3',
                    equalTo: "Must be unique"
                },

            },
            errorPlacement: function (label, element) {
                if (element.is("textarea")) {
                    label.insertAfter(element.next());
                } else {
                    label.insertAfter(element)
                }
            },
            submitHandler: function (form) {
                var product = new FormData($("#insertproduct")[0]);
                //alert(product);
                $.ajax({
                    url: '<?= site_url('products/insert_product') ?>',
                    type: 'POST',
                    data: product,
                    success: function (data) {
//                        alert(data);
                        location.assign('https://mtpromet.novakov.mk/products');


                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
            }
        });
    });
</script>
