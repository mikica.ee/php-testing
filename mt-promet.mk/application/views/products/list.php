<div class="dashboard-wrapper">
    <div class="container-fluid  dashboard-content">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title text-center">Product List</h2>
                </div>
            </div>
        </div>
        <div class="row text-center" >    
            <div class="row pt-3 pt-sm-3 mt-3">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 offset-1">
                    <form method="post" action="products/insert">
                        <button type="submit" class="btn btn-space btn-primary">Add Product</button>
                        <button type="button" id="deleteAcc" class="btn btn-space btn-secondary">Mass Delete</button>
                    </form>
                </div>
            </div>
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="row">
                    <?php foreach ($products as $key => $value) { ?>

                        <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
                            <div class="product-thumbnail">
                                <div class="product-content">  
                                    <div class="product-content-head">
                                        <div class="checkbox">
                                            <label class="checkbox">
                                                <input  value="<?= $value['product_id'] ?>" type="checkbox"></label>
                                        </div>
                                        <a href="javascript:;" id="delete" product_id = "<?= $value['product_id'] ?>"><span class="fas fa-trash"></span></a>
                                        <h3 class="product_slug"><?= $value['product_slug'] ?></h3>
                                        <h3 class="product-title"><?= $value['product_name'] ?></h3>
                                        <div class="product_price"><?= $value['product_price'] ?> $</div>
                                        <div class="product_desc"><?= $value['product_desc'] ?></div>
                                        <div class="product_desc1"><?= $value['product_desc1'] ?></div>
                                        <div class="product_desc2"><?= $value['product_desc2'] ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function ($) {
        $("#deleteAcc").on("click", function () {
            $(".checkbox input:checked").parent().remove();
        });
        $("a#delete").click(function (e) {
            var product_id = $(this).attr("product_id");
            var obj = {
                product_id: product_id
            }
            if (confirm("Delete?")) {
                $.ajax({
                    url: '<?= site_url('products/delete') ?>',
                    type: 'POST',
                    data: obj,
                    success: function (data) {
                        alert(data);
                        location.reload();
                    },
                });
            }
        });
    });
</script>